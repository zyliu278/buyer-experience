## Navigation Release Checklist
To be used for major and minor navigation releases.

**Reviewer Checklist:**

_Chrome_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially "Cookie Preferences" button, and Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

_Safari_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially "Cookie Preferences" button, and Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

_Firefox_
- [ ] Test a link in each dropdown of the **desktop** header (Especially login/free trial buttons)
- [ ] Test a link each section of the **desktop** footer (Especially "Cookie Preferences" button, and Edit in IDE/Page source links)
- [ ] Test a link in each section of the **mobile** header
- [ ] Test a link in each section of the **mobile** footer
- [ ] Test search on desktop
- [ ] Test search on mobile
- [ ] Test tabbing through header links
- [ ] Test tabbing through footer links

